/*
    This code handles the strcpy buffer overflow example. If you uncomment the NO_OVERFLOW_CPY the code will crash due to buffer overflow.
	The #pragma is used to replicate the problem disabling the strcpy warning. Also other functions are considered unsafe like strcmp(buff, password) 
	and gets(buff). These can be fixed using strncmp(buff, password, pwdSize) and fgets(buff, sizeof(buff), stdin) respectively.

	https://stackoverflow.com/questions/3302255/c-scanf-vs-gets-vs-fgets
	http://www.cplusplus.com/reference/cstring/strncmp/
	http://www.cplusplus.com/reference/cstring/strcpy/
	http://www.cplusplus.com/reference/cstring/strncpy/
	https://stackoverflow.com/questions/869883/why-is-strncpy-insecure

*/

#pragma warning(disable: 4996) //4996 for _CRT_SECURE_NO_WARNINGS equivalent

#include <stdio.h>
#include <string.h>

#define DEBUG 1
#define NO_OVERFLOW_CPY 1
#define MAX 15

int main()
{
	char temp[MAX];
	char buff[] = "0123456789abcdefghijklmnopqrstuvwxyz";
	char password[] = "thegeekstuff";
	int pass = 0;

	//Remove Null terminiator
	unsigned char pwdSize = sizeof(password) - 1;

#ifdef NO_OVERFLOW_CPY
	strncpy(temp, buff, MAX);
#else
	strcpy(temp, buff);
#endif

	if (strncmp(buff, password, pwdSize) == 0)
	{
		printf("\n Correct Password \n");
		pass = 1;
	}
	else
	{
		printf("\n Wrong Password \n");
	}

	if (pass)
	{
		/* Now Give root or admin rights to user*/
		printf("\n Root privileges given to the user \n");
	}

#ifdef DEBUG
	printf ("\n\nbuff: %s\ntemp: %s\npass: %s\n",buff,temp,password);
#endif

	return 0;
}
